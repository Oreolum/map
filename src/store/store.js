import Vue from 'vue'

export const store = Vue.observable({
    stateLocation: 0
})

export const storeMutations = {
    setStateLocation(value) { 
        store.stateLocation = value
    }
}