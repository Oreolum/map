import Vue from 'vue'
import App from './map'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'map',
      component: App,
    },
  ],
})

Vue.use(VueAxios, axios)

new Vue({
  router,
  el: '#app',
  render: (h) => h(App),
})
